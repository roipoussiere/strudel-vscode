# Strudel extension for VSCode

If you want to contribute to Strudel itself (ie. how to convert text into music), you must refer to the [https://github.com/tidalcycles/strudel](Strudel repository).

## Sending feedback

They are highly appreciated! You can either:

- create issues and suggestions on [Framagit](https://framagit.org/roipoussiere/strudel-vscode/-/issues) (GitLab instance);
- say hello to `@roipoussiere` on the Strudel Discord :)

## Contributing to code

To download the project and its dependencies:

```
git clone https://framagit.org/roipoussiere/strudel-vscode
cd strudel-vscode
npm install
```

You can now test the extension by hitting `F5`.

To generate the `.vsix` file:

    npm run pack

Take a look at the script entries in the `package.json` for more advanced commands.
