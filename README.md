# Strudel extension for VSCode

A VSCode/VSCodium extension to write and run [Strudel](https://strudel.tidalcycles.org/) tunes for live coding.

![](./images/full_screenshot.png)

## Installation

### Via VSCode IDE

Search for `strudel` in the extensions tab and click `install`.

Alternatively you can open the command palette (`ctrl+shift+p`) and type `ext install roipoussiere.strudel-vscode`.

### Via `.vsix` file

Open the command palette type `vsix` and select *Extension: Install from VSIX*.

The vsix file can be either:
- dowloaded from the [extension page](https://marketplace.visualstudio.com/items?itemName=roipoussiere.strudel-vscode) in the VSCode marketplace (`Download extension` in the side bar);
- built from sources (see [code section](#code) below).

## Usage

Once the Strudel extension is installed, you can create or open some tunes. You must name them using the `.std` extension in order to allow the VSCode extension to recognize them.

![](images/explorer.png)

Then click on the `Connect audio` button in the Strudel panel (exporer tab).

![](images/connect.png)

Great! Now you can play, stop or upload your tunes, using the buttons on the upper-right corner.

![](images/play.png) ![](images/stop.png)

You can also use keyboard shortcuts:
- `ctrl-enter`: play, update;
- `ctrl-.`: stop

Those are configurable in VSCode settings.

You can also look at the Output panel for the Strudel console:

![](./images/logs.png)

## Features

- attempt to integrate as well as possible with the VSCode UI;
- display piano roll;
- basic syntax highligting;
- colorized Strudel console in bottom bar;
- allow to only play selection.

<!-- ## Extension Settings -->

## Contributing

See [CONTRIBUTING.md](contribution guide)

## Authorship

### Extension source code

- credits: Nathanaël Jourdane and contributors
- license: [AGPL-3.0](./LICENSE)
- source: https://framagit.org/roipoussiere/strudel-vscode

### Strudel engine

- credits: Strudel contributors
- license: [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0.txt)
- source: https://github.com/tidalcycles/strudel

### Pre-loaded sounds

- piano:
  - credits: Alexander Holm
  - license: [CC-by](http://creativecommons.org/licenses/by/3.0)
  - source: https://archive.org/details/SalamanderGrandPianoV3
- VCSL:
  - credits: Versilian Studios LLC
  - license: [CC0](https://creativecommons.org/publicdomain/zero/1.0/)
  - source: https://github.com/sgossner/VCSL
- Tidal drum machines:
  - source: https://github.com/ritchse/tidal-drum-machines
- EmuSP12:
  - source: https://github.com/tidalcycles/Dirt-Samples
