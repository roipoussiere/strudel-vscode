import * as core from '@strudel.cycles/core';
import * as mini from '@strudel.cycles/mini';
import * as webaudio from '@strudel.cycles/webaudio';
import * as tonal from '@strudel.cycles/tonal';
import { registerSoundfonts } from '@strudel.cycles/soundfonts';
import { transpiler } from '@strudel.cycles/transpiler';

const vscode = acquireVsCodeApi();
const ctx = webaudio.getAudioContext();

const domConnectBtn = document.getElementById('strudel-connect');
const domInfoText = document.getElementById('strudel-info');

console.defaultLog = console.log.bind(console);
console.logs = [];

webaudio.initAudioOnFirstClick();
core.evalScope(core.controls, core, mini, webaudio, tonal);

console.log = function() {
    console.defaultLog.apply(console, arguments);
    console.logs.push(Array.from(arguments));
	vscode.postMessage({
		type: 'log',
		value: arguments[0]
	});
};

export async function prebake() {
	await Promise.all([
		webaudio.registerSynthSounds(),
		registerSoundfonts(),

		webaudio.samples('https://strudel.tidalcycles.org/piano.json',
			'https://strudel.tidalcycles.org/piano/',
			{ prebake: true }),
		webaudio.samples('https://strudel.tidalcycles.org/vcsl.json',
			'github:sgossner/VCSL/master/',
			{ prebake: true }),
		webaudio.samples('https://strudel.tidalcycles.org/tidal-drum-machines.json',
			'github:ritchse/tidal-drum-machines/main/machines/',
			{ prebake: true, tag: 'drum-machines' }),
		webaudio.samples('https://strudel.tidalcycles.org/EmuSP12.json',
			'https://strudel.tidalcycles.org/EmuSP12/',
			{ prebake: true, tag: 'drum-machines' })
	]);
}

// return: {scheduler, evaluate, start, stop, pause, setCps, setPattern}
const repl = core.repl({
	defaultOutput: webaudio.webaudioOutput,
	getTime: () => ctx.currentTime,
	transpiler
});

const initSound = async () => {
	webaudio.resetLoadedSounds();
	repl.scheduler.setCps(1);
	await prebake();
};

let tune = ' ';

function connect() {
	try {
		initSound();
		repl.setPattern(' ');
		repl.start();
		vscode.postMessage({
			type: 'status',
			value: 'connected'
		});
		return true;
	} catch (error) {
		vscode.postMessage({
			type: 'error',
			value: error.message
		});
	}
	return false;
}

function play() {
	if (webaudio.getAudioContext().state !== 'running') {
		vscode.postMessage({
			type: 'status',
			value: 'disconnected'
		});
		return false;
	}

	try {
		repl.evaluate(tune);
		repl.start();
		domInfoText.style.display = 'none';
		showPianoRoll(true);
		vscode.postMessage({
			type: 'status',
			value: 'playing'
		});
		return true;
	} catch (error) {
		vscode.postMessage({
			type: 'error',
			value: error.message
		});
	}
}

function stop() {
	try {
		repl.stop();
		showPianoRoll(false);
		vscode.postMessage({
			type: 'status',
			value: 'stopped'
		});
		return true;
	} catch (error) {
		vscode.postMessage({
			type: 'error',
			value: error.message
		});
	}
	return false;
}

function showPianoRoll(shouldShow) {
	const domPianoroll = document.getElementById('test-canvas');
	if (domPianoroll) {
		domPianoroll.style.display = shouldShow ? 'block' : 'none';
	}
}

domConnectBtn.addEventListener('click', event => {
	if ( connect(event.target) ) {
		event.target.style.display = 'none';
		domInfoText.style.display = 'block';
		domInfoText.innerText = '✅ Audio Connected';
	}
});

window.addEventListener('message', event => {
	const message = event.data;
	switch (message.command) {
		case 'play':
			play();
			break;
		case 'update':
			tune = message.data;
			play();
			break;
		case 'stop':
			stop();
			break;
		}
});
