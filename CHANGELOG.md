# Changelog

## 0.2.1

- add context menu entry to load a tune example
- convert to a web extension
- several fixes
- refactoring

## 0.2 (beta)

- rename *Tidal Strudel* to avoid confusion with other extensions
- improve status bar
- add *play selection* option in context menu
- improve Strudel view to better show the piano roll
- load default sound files
- make file extension customisable via VSCode IDE (`.strudel` and `.str` by default)
- improve docs
- switch to agpl3 license
- some refactoring

## 0.1 (alpha)

- play active document
- show Strudel UI (including pianoroll) in a view (aka sidebar) instead a panel
- add buttons bar
- add logger
- add status bar
- add syntax highlighting
- setup a build workflow
- publish on VSCode marketplace

## 0.0.1 (prototype)

- display the Strudel repl in a webview panel that can play music
