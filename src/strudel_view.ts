import * as vscode from 'vscode';

export type OnEvent = ((message: string) => void);

class StrudelView implements vscode.WebviewViewProvider {

	public static readonly viewType = 'strudel.strudelView';

	public onReceiveStatus: OnEvent;
    public onReceiveWarning: OnEvent;
    public onReceiveError: OnEvent;
    public onReceiveLog: OnEvent;

	private _view?: vscode.WebviewView;

	constructor(private readonly _extensionUri: vscode.Uri) {
		this.onReceiveStatus = () => { throw new Error('Not implemented'); };
		this.onReceiveWarning = () => { throw new Error('Not implemented'); };
		this.onReceiveError = () => { throw new Error('Not implemented'); };
		this.onReceiveLog = () => { throw new Error('Not implemented'); };		
    }

	public resolveWebviewView(webviewView: vscode.WebviewView): void {
		this._view = webviewView;

		webviewView.webview.options = {
			enableScripts: true,
			localResourceRoots: [ this._extensionUri ]
		};

		webviewView.webview.html = this.getWebviewHtml(webviewView.webview);
		webviewView.webview.onDidReceiveMessage(msg => {
			switch (msg.type) {
				case 'status':
					this.onReceiveStatus(msg.value);
					return;
				case 'warning':
					this.onReceiveWarning(msg.value);
					return;
				case 'error':
					this.onReceiveError(msg.value);
					return;
				case 'log':
					this.onReceiveLog(msg.value);
					return;
				default:
					throw new Error(`Received unknown message type: ${ msg.type }`);
			}
		});
	}

	public updateTune(data: string): void {
		if (this._view) {
			this._view.webview.postMessage({ command: 'update', data });
		}
	}

	public playTune(): void {
		if (this._view) {
            this._view.show();
			this._view.webview.postMessage({ command: 'play' });
		}
	}

	public stopTune(): void {
		if (this._view) {
			this._view.webview.postMessage({ command: 'stop' });
		}
	}

	private getWebviewHtml(webview: vscode.Webview): string {
		const scriptPathOnDisk = vscode.Uri.joinPath(this._extensionUri, 'dist', 'strudel.js');
		const scriptUri = webview.asWebviewUri(scriptPathOnDisk);

		const stylePathMainPath = vscode.Uri.joinPath(this._extensionUri, 'static', 'strudel.css');
		const styleUri = webview.asWebviewUri(stylePathMainPath);

		return `<!DOCTYPE html>
<html lang="en">
	<head>
		<link href="${styleUri}" rel="stylesheet">
	</head>
	<body>
		<main>
			<button class="center" id="strudel-connect">🔌 Connect audio</button>
			<p class="center" id="strudel-info" style="display: none"></p>
		</main>
		<script type="module" src="${scriptUri}"></script>
	</body>
</html>
		`;
	}
}

export default StrudelView;
