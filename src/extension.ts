import * as vscode from 'vscode';

import Strudel from './strudel';
import StrudelView from './strudel_view';

export function activate(context: vscode.ExtensionContext): void {
	const strudelView = new StrudelView(context.extensionUri);
	const strudel = new Strudel(strudelView, context.extensionUri);
	strudel.init();

	context.subscriptions.push(strudel.strudelStatusBar);

	context.subscriptions.push(vscode.window.registerWebviewViewProvider(
		StrudelView.viewType, strudelView));

	context.subscriptions.push(vscode.commands.registerCommand(
		'strudel.play', () => strudel.play()));

	context.subscriptions.push(vscode.commands.registerCommand(
		'strudel.play_selection', () => strudel.playSelection));

	context.subscriptions.push(vscode.commands.registerCommand(
		'strudel.update', () => strudel.update()));

	context.subscriptions.push(vscode.commands.registerCommand(
		'strudel.stop', () => strudel.stop()));

	context.subscriptions.push(vscode.commands.registerCommand(
		'strudel.load_examples', () => strudel.loadExamples()));
	
}
